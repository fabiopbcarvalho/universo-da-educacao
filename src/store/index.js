import Vue from 'vue'
import Vuex from 'vuex'

import {adm} from './modules/adm'

Vue.use(Vuex)

export default new Vuex.Store({

  modules: {
    adm
  },
  
  // state: {
  // },
  // mutations: {
  // },
  // actions: {
  // },
})
