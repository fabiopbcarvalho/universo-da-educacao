export const mutations = {
    setLogin(state, login) {
        state.login = login
        window.localStorage.setItem('login', JSON.stringify(login))
    },
    getLogin(state) {
        return state.login
        // window.localStorage.setItem('login', JSON.stringify(login))
    },
    setLogout(state) {
        state.login = null
        window.localStorage.removeItem('login')
    },
    setLoading(state, show) {
        state.loading = show
    },
    // setSelectedItemMenuPrincipal(state, opcao) {
    //     window.localStorage.setItem('selectedItemMenuPrincipal', opcao)
    //     state.selectedItemMenuPrincipal = opcao
    // },
    setError(state, error) {
        state.error.show = error.show
        state.error.message = error.message
    },
    resetError(state) {
        state.error.show = false
        state.error.message = null
    },
    setSearchText(state, texto) {
        state.searchText = texto
    },
    setModulo(state, modulo) {
        state.selectedModule = modulo
    },


};