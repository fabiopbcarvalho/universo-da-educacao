import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'

const state = {
    // login: JSON.parse(window.localStorage.getItem('login')) || null
    searchText: JSON.parse(window.localStorage.getItem('searchText')) || '',
    selectedModule: JSON.parse(window.localStorage.getItem('selectedModule')) || '',
    selectedBgColor: JSON.parse(window.localStorage.getItem('selectedBgColor')) || '',
    login: null,
    loading: false,
    error: {
        show: false,
        message: ''
    },
    // selectedItemMenuPrincipal: window.localStorage.getItem('selectedItemMenuPrincipal') || null
};

const namespaced = true;

export const adm = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};