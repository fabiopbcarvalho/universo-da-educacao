export const getters = {
    login: state => {
        return state.login
    },
    getLoading: state => {
        return state.loading
    },
    getSelectedItemMenuPrincipal: state => {
        return state.selectedItemMenuPrincipal 
    },
    getShowError: state => {
        return state.error.show
    },
    getMessageError: state => {
        return state.error.message
    },
    getSearchText: state => {
        return state.searchText
    },
    getModulo: state => {
        return state.selectedModule
    },
    
};