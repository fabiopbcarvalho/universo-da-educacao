import dbModulos from '@/database/json/SIGEAM-Menus.json'
// import GrauEnsino from '@/db/statics/grauEnsino'

//import axios from '@/axios-auth'

const ESPERA = 100

export const actions = {
    login(context, login) {
        return new Promise((resolve) => {
            setTimeout(() => {
            }, ESPERA);
            context.commit('setLogin', login);
            resolve('ok');                
        })
        // this.axios.get('/api/users')
        //     .then((response) => {
        //         let users = response.data.data;

        //         context.commit('setUsers', users);
        //     });
    },
    
    logout(context) {
        setTimeout(() => {
            context.commit('setLogin', null);
        }, ESPERA);
    },
    
    setLoading({commit}, payload) {
        commit('setLoading', payload)
    },

    setSearchText({commit}, texto) {
        return new Promise((resolve) => {
            // setTimeout(() => {
            //     console.log('setSearchText')
            // }, 3000);
            commit('setSearchText', texto)
            resolve(true)
        })
    },

    getModulos(context) {
        return new Promise((resolve) => {
            // setTimeout(() => {
            //     console.log('setSearchText')
            // }, 3000);
            resolve(dbModulos)
        })
    },

    setModulo(context, payload) {
        return new Promise((resolve) => {
            resolve(payload)
        })
    },

};